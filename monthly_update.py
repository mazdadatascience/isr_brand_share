import pandas as pd
import os
import subprocess
import datetime

from sqlalchemy import create_engine

os.environ["NLS_LANG"] = ".AL32UTF8"

engine = create_engine(
    "oracle+cx_oracle://RPR_STG:rpSgdEv12@x4ebid10.mnao.net:1521/EDWHDEV",
    max_identifier_length=128,
)

isr_query = """\
SELECT
	*
FROM
	(
	SELECT
		YEAR_MONTH,
		NAMEPLATE,
        MODEL,
		MAZDA_SUBSEGMENT,
		CUSTOMER_FACING_TRANSACTION_PRICE,
		MAZDA_REGION,
		WEIGHTED_COUNT
	FROM
		RPR_STG.JB_ISR_MARKETS
	WHERE
		GRANULARITY LIKE 'Model'
		AND MODEL_YEAR LIKE 'All MY') t1
LEFT JOIN (
	SELECT DISTINCT
		CAL_YR_MO_ID,
		FIS_YR_MAZDA_ID
	FROM
		EDW_STG.KHN_CALENDAR_MASTER) t2 ON
	t1.YEAR_MONTH = t2.CAL_YR_MO_ID"""

isr_df = pd.read_sql(isr_query, engine)

isr_df.to_csv("data/fact_isr_df.csv", index=False)

#### SYNC ONEDRIVE ####
sync_cmd = "rclone sync -v data sharepoint:wphyo/isr_brand_share"
with open("logging.txt", "w") as f:
    process = subprocess.Popen(sync_cmd, shell=True, stderr=f, universal_newlines=True)
    while True:
        return_code = process.poll()
        if return_code is not None:
            break
#### SYNC ONEDRIVE ####
